<?php

$db1 = "myusers_dba";
$db2 = "myusers";
// Create connection to Oracle
$conn = oci_connect("myusers_dba", "gt", "//localhost/orcl");
if (!$conn) {
   $m = oci_error();
   echo $m['message'], "\n";
   exit;
}
echo "link established to oracle with schema $db1<br>";

$mysql_conn = mysqli_connect('localhost', 'root', 'gt', $db2);
if(!$mysql_conn) {
	die("Cannot connect to mysql. ".mysqli_connect_error());
}
echo "link established to mysql with database $db2<br><br>";



$dblink = 'myusers_dba_mylink';
$dblink_drop_query = "BEGIN
                        EXECUTE IMMEDIATE 'DROP PUBLIC DATABASE LINK ".$dblink."';
                      EXCEPTION
                        WHEN OTHERS THEN
                          IF SQLCODE != -2024 THEN
                            RAISE;
                           END IF;
                      END;";
// echo $dblink_drop_query;
$dblink_query = "CREATE PUBLIC DATABASE LINK $dblink CONNECT TO \"root\" IDENTIFIED BY \"gt\" USING 'MYSQL'";
$dblinkres = oci_parse($conn, $dblink_drop_query);
oci_execute($dblinkres);
// die("<br><br>drop query executed");
$dblink_res = oci_parse($conn, $dblink_query);
oci_execute($dblink_res);

$query = "select table_name from user_tables";
$stid = oci_parse($conn, $query);
$r = oci_execute($stid);



while ($row = oci_fetch_array($stid)) {
	$table = $row['TABLE_NAME'];

	$val = mysqli_query($mysql_conn, "select 1 from $table LIMIT 1");

	if($val === FALSE)
	{
	   echo "<br>$table not exists in mysql..skipping..<br>";
	   continue;
	}


	// if (strpos($table, "$") !== false) {continue;}
	echo "<h2>".$table."</h2>";
	$query = "select * from user_tab_cols where table_name='$table'";
	$col_res = oci_parse($conn, $query);
	oci_execute($col_res);
	$columns = [];
	$values = [];
	$upd_vals = [];
	while($col_row = oci_fetch_array($col_res)) {
		$col = strtolower($col_row['COLUMN_NAME']);
		$columns[] = $col;
		// echo "<br><br><br>";
		$values[] = ':NEW.'.$col;
		$upd_vals[] = "\"".$col."\"=:NEW.".$col;
	}
	echo "<b>columns</b> : ".implode(", ", $columns)."<br>";
	echo "<b>values to be inserted</b>(for insert trigger) : (".implode(", ", $values).")<br>";
	echo "<b>values to be updated</b>(for update trigger) : ".implode(", ", $upd_vals)."<br>";
	
	$query = "SELECT cols.table_name, cols.column_name, cols.position, cons.status, cons.owner
		FROM user_constraints cons, user_cons_columns cols
		WHERE cols.table_name = '$table'
		AND cons.constraint_type = 'P'
		AND cons.constraint_name = cols.constraint_name
		AND cons.owner = cols.owner
		ORDER BY cols.table_name, cols.position";

	$p_key_res = oci_parse($conn, $query);
	oci_execute($p_key_res);
	$p_keys = [];
	$constr = [];
	while($col_row = oci_fetch_array($p_key_res)) {
		$col = strtolower($col_row['COLUMN_NAME']);
		$p_keys[] = $col;
		$constr[] = ":OLD.".$col."=\"".$col."\"";
		
		// echo "<br><br><br>";
	}
	echo "constraints: ".implode(" and ", $constr)."<br>";

	$insert_trigger = "CREATE OR REPLACE TRIGGER INSERT_ON_$table AFTER INSERT ON $db1.$table ".
	                    // "REFERENCING OLD AS OLD NEW AS NEW ".
	                    "FOR EACH ROW ".
						"DECLARE ".
                            "PRAGMA AUTONOMOUS_TRANSACTION; ".
						"BEGIN ".
							"INSERT INTO $table@$dblink VALUES(".implode(", ", $values)."); ".
							"COMMIT; ".
						"END; ";

	$delete_trigger = "CREATE OR REPLACE TRIGGER DELETE_ON_$table AFTER DELETE ON $db1.$table ".
	                    // "REFERENCING OLD AS OLD NEW AS NEW ".
	                    "FOR EACH ROW ".
						"DECLARE ".
                            "PRAGMA AUTONOMOUS_TRANSACTION; ".
						"BEGIN ".
							"DELETE FROM $table@$dblink WHERE ".implode(" and ", $constr)."; ".
							"COMMIT; ".
						"END; ";




	$update_trigger = "CREATE OR REPLACE TRIGGER UPDATE_ON_$table AFTER update ON $db1.$table ".
	                    // "REFERENCING OLD AS OLD NEW AS NEW ".
	                    "FOR EACH ROW ".
						"DECLARE ".
                            "PRAGMA AUTONOMOUS_TRANSACTION; ".
						"BEGIN ".
							"UPDATE $table@$dblink SET ".implode(", ", $upd_vals)." WHERE ".implode(" and ", $constr).";" .
							"COMMIT; ".
						"END; ";

	echo "<br>----------------------------<br>";
	echo $insert_trigger."<br>";
	echo "<br><br>";
	// echo "<br><br><br>";
	echo $delete_trigger."<br>";
	echo "<br><br>";
	// echo "<br><br><br>";
	echo $update_trigger."<br>";


	// $insert_trigger_query = oci_parse($conn, $insert_trigger);
	// oci_execute($insert_trigger_query);

	// $delete_trigger_query = oci_parse($conn, $delete_trigger);
	// oci_execute($delete_trigger_query);

	// $update_trigger_query = oci_parse($conn, $update_trigger);
	// oci_execute($update_trigger_query);
	echo "<br><br>***************************************<br><br>";
}

echo "Hello World";
// Close the Oracle connection
oci_close($conn);

?>