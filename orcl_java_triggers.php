<?php

// echo "'INSERT INTO users values(' || :NEW.id || ', ''' || :NEW.mail || ''', ''' || :NEW.password || '', ' || :NEW.otp || ')'";
// die();
// Create connection to Oracle
$conn = oci_connect("myusers_dba", "gt", "//localhost/orcl");
if (!$conn) {
   $m = oci_error();
   echo $m['message'], "\n";
   exit;
}
$db1 = "MYUSERS_DBA";
$db2 = "myusers";

// echo $dblink_drop_query;
// $stored_java_procd_query = "CREATE OR REPLACE PROCEDURE ";
// $dblinkres = oci_parse($conn, $dblink_drop_query);
// oci_execute($dblinkres);
// // die("<br><br>drop query executed");
// $dblink_res = oci_parse($conn, $dblink_query);
// oci_execute($dblink_res);

$query = "select table_name from user_tables";
$stid = oci_parse($conn, $query);
$r = oci_execute($stid);



while ($row = oci_fetch_array($stid)) {
	$table = $row['TABLE_NAME'];
	if (strpos($table, "$") !== false) {continue;}
	echo "<h2>".$table."</h2>";
	$query = "select column_name, data_type, nullable from user_tab_cols where table_name='$table'";
	$col_res = oci_parse($conn, $query);
	oci_execute($col_res);
	$columns = [];
	$values = "'";
	$value = "";
	$upd_vals = "'";
	while($col_row = oci_fetch_array($col_res)) {
		$col = strtolower($col_row['COLUMN_NAME']);
		$data_type = strtolower($col_row['DATA_TYPE']);
		$nullable = strtolower($col_row['NULLABLE']);
		$columns[] = $col;
		// echo "<br><br><br>";
		if ($nullable==='y') {
			$value = "CASE WHEN :NEW.$col IS NULL THEN 'NULL' ELSE '' || :NEW.$col || '' END";
		}
		else {
			$value = ":NEW.$col";
		}
		if (strpos($data_type,"char")!==false) { 
			$values .= " ''' || ".$value." || ''', ";
		}
		else {
			$values .= " ' || ".$value." || ', ";
		}
		$upd_vals .= " \"$col\"=' || ".$value." || ', ";	// $col=:NEW.$col
	}
	echo "<b>columns</b> : ".implode(", ", $columns)."<br>";
	$values = substr($values,0,-6);//"|| ',");
	$upd_vals = substr($upd_vals,0,-6);// "|| ', ");
	echo "<b>values to be inserted</b>(for insert trigger) : ".$values."<br>";
	echo "<b>values to be updated</b>(for update trigger) : ".$upd_vals."<br>";
	
	$query = "SELECT cols.table_name, cols.column_name, cols.position, cons.status, cons.owner
		FROM user_constraints cons, user_cons_columns cols
		WHERE cols.table_name = '$table'
		AND cons.constraint_type = 'P'
		AND cons.constraint_name = cols.constraint_name
		AND cons.owner = cols.owner
		ORDER BY cols.table_name, cols.position";

	$p_key_res = oci_parse($conn, $query);
	oci_execute($p_key_res);
	$p_keys = [];
	$constr = "'";
	while($col_row = oci_fetch_array($p_key_res)) {
		$col = strtolower($col_row['COLUMN_NAME']);
		$p_keys[] = $col;
		$constr .= " \"".$col."\"=' || :OLD.".$col." || ' and ";
		
		// echo "<br><br><br>";
	}
	$constr = substr($constr,0, -9);//"|| ' and ")."'";
	echo "<b>constraints</b>(for update and delete) : ".$constr."<br>";

	echo "<br>------------------------------<br><br>";

	$insert_trigger = "CREATE OR REPLACE TRIGGER INSERT_ON_$table AFTER INSERT ON $db1.$table ".
	                   "FOR EACH ROW ".
	                   "CALL proc_$table('INSERT INTO ".$table." VALUES(' || ".$values." || ')'); ";


	$delete_trigger = "CREATE OR REPLACE TRIGGER DELETE_ON_$table AFTER DELETE ON $db1.$table ".
	                    "FOR EACH ROW ".
						"BEGIN ".
							"CALL DBTrigger.update_mysql('DELETE FROM $table WHERE ' || ".$constr."); ".
						"END; ";




	$update_trigger = "CREATE OR REPLACE TRIGGER UPDATE_ON_$table AFTER update ON $db1.$table ".
	                    "FOR EACH ROW ".
						"BEGIN ".
							"CALL DBTrigger.update_mysql('UPDATE $table SET ' || ".$upd_vals." || ' WHERE ' || ".$constr.");" .
						"END; ";

	
	echo $insert_trigger."<br>";
	echo "<br><br>";
	// echo "<br><br><br>";
	echo $delete_trigger."<br>";
	echo "<br><br>";
	// echo "<br><br><br>";
	echo $update_trigger."<br>";


	$insert_trigger_query = oci_parse($conn, $insert_trigger);
	oci_execute($insert_trigger_query);
echo "********";
	$delete_trigger_query = oci_parse($conn, $delete_trigger);
	oci_execute($delete_trigger_query);
echo "*********";
	$update_trigger_query = oci_parse($conn, $update_trigger);
	oci_execute($update_trigger_query);
	echo "<br><br>***************************************<br><br>";
}

echo "Hello World";
// Close the Oracle connection
oci_close($conn);

?>
<!-- CREATE OR REPLACE TRIGGER insert_on_users AFTER INSERT ON USERS
FOR EACH ROW
BEGIN
	dbms_output.put_line('INSERT INTO users values(' || :NEW.id || ', ''' || :NEW.mail || ''', ''' || :NEW.password || ''', ' || CASE WHEN :NEW.otp IS NULL THEN 'NULL' ELSE '' || :NEW.otp || '' END || ')');
END; -->
<!-- 
CREATE OR REPLACE PROCEDURE log_sal (
  emp_id NUMBER, old_sal NUMBER, new_sal NUMBER)
AS LANGUAGE JAVA
NAME 'DBTrigger.logSal(int, float, float)';
 -->
<!-- CREATE OR REPLACE PROCEDURE proc_users (query VARCHAR2)
AS LANGUAGE JAVA
NAME 'DBTrigger.update_mysql(java.lang.String)'; -->