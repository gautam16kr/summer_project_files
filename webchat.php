<!DOCTYPE html>
<html>
<head>
  <title>Web Chat App</title>
  <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase.js"></script>
  <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-app.js"></script>
  <!-- Add additional services you want to use -->
  <!-- <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-auth.js"></script> -->
  <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-database.js"></script>
  <!-- <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-firestore.js"></script> -->
  <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-messaging.js"></script>
  <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-functions.js"></script>

  <!-- Comment out (or don't include) services you don't want to use -->
  <!-- <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-storage.js"></script> -->

  <script>
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyCvLlIXu6XvBpKCt75jsFTmuuf6T4r2_U0",
      authDomain: "web-chat-app-7bdb5.firebaseapp.com",
      databaseURL: "https://web-chat-app-7bdb5.firebaseio.com",
      projectId: "web-chat-app-7bdb5",
      storageBucket: "",
      messagingSenderId: "85728220170"
    };
    firebase.initializeApp(config);
  </script>
</head>
<body>

</body>
</html>