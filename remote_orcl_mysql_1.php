<?php

// $db1 = "myusers_dba";
// Create connection to Oracle
$orcl_user = 'system';
$orcl_passwd = 'KOT3JLCF94';
$conn_string = '//kramah.maxapex.net/XE';
$schema = 'NDCNEW';

// $orcl_user = 'myusers_dba';
// $orcl_passwd = 'gt';
// $conn_string = '//localhost/orcl';
// $schema = 'myusers_dba';
$conn = oci_connect($orcl_user, $orcl_passwd, $conn_string);
if (!$conn) {
   $m = oci_error();
   echo $m['message'], "\n";
   exit;
}
else {
	echo "Connection established to oracle @ $conn_string<br>";
}
// echo "link established to oracle with schema $db1<br>";

$mysql_host = 'sql45.main-hosting.eu';
$mysql_user = 'u810528275_autom';
$mysql_passwd = 'kramah1234';
$db = 'u810528275_autom';

// $mysql_host = 'sql12.freemysqlhosting.net';
// $mysql_user = 'sql12236898';
// $mysql_passwd = 'PaFRWLtcPI';
// $db = 'sql12236898';
// $mysql_conn = mysqli_connect($mysql_host, $mysql_user, $mysql_passwd, $db);
// if(!$mysql_conn) {
// 	die("Cannot connect to mysql. ".mysqli_connect_error());
// }
// echo "Connection established to mysql @ $mysql_host database $db<br><br>";


$dblink = 'autom_dba_mylink';
$dblink_drop_query = "BEGIN
                        EXECUTE IMMEDIATE 'DROP PUBLIC DATABASE LINK ".$dblink."';
                      EXCEPTION
                        WHEN OTHERS THEN
                          IF SQLCODE != -2024 THEN
                            RAISE;
                           END IF;
                      END;";
// echo $dblink_drop_query;
$conn_iden = 'mysql';
$dblink_query = "CREATE PUBLIC DATABASE LINK $dblink CONNECT TO \"$mysql_user\" IDENTIFIED BY \"$mysql_passwd\" USING '".$conn_iden."'";
echo $dblink_query."<br>";
$dblinkres = oci_parse($conn, $dblink_drop_query);
oci_execute($dblinkres);
// die("<br><br>drop query executed");
$dblink_res = oci_parse($conn, $dblink_query);
oci_execute($dblink_res);

$query = "select * from all_tables where table_name='AUTOM'";
echo "$query<br>";
$stid = oci_parse($conn, $query);
$r = oci_execute($stid);
// print_r($stid);

while ($row = oci_fetch_array($stid)) {
	$table = $row['TABLE_NAME'];

	// $val = mysqli_query($mysql_conn, "select 1 from $table LIMIT 1");
	// if($val === FALSE) {
	//    echo "<br>$table not exists in mysql..skipping..<br>";
	//    continue;
	// }

	// if (strpos($table, "$") !== false) {continue;}
	echo "<h2>".$table."</h2>";
	// print_r($row);
	// echo "<br><br>";

// 	// if (strpos($table, "$") !== false) {continue;}
	$query = "select column_name, data_type, nullable from all_tab_cols where table_name='$table'";
	$col_res = oci_parse($conn, $query);
	oci_execute($col_res);
	$columns = [];
	$values = "'";
	$value = "";
	$upd_vals = "'";
	while($col_row = oci_fetch_array($col_res)) {
		$col = strtoupper($col_row['COLUMN_NAME']);
		$data_type = strtoupper($col_row['DATA_TYPE']);
		$nullable = strtoupper($col_row['NULLABLE']);
		$columns[] = "\"".$col."\"";
		// echo "<br><br><br>";
		if ($nullable==='Y') {
			$value = "CASE WHEN :NEW.$col IS NULL THEN 'NULL' ELSE '' || :NEW.$col || '' END";
		}
		else {
			$value = ":NEW.$col";
		}
		if (strpos($data_type,"CHAR")!==false) { 
			$values .= " ''' || ".$value." || ''', ";
			$upd_vals .= " \"$col\"=''' || ".$value." || ''', ";
		}
		else {
			$values .= " ' || ".$value." || ', ";
			$upd_vals .= " \"$col\"=' || ".$value." || ', ";
		}
		// $upd_vals .= " \"$col\"=' || ".$value." || ', ";	// $col=:NEW.$col
	}
	echo "<b>columns</b> : ".implode(", ", $columns)."<br>";
	$values = substr($values,0,-6);//"|| ',");
	$upd_vals = substr($upd_vals,0,-6);// "|| ', ");
	echo "<b>values to be inserted</b>(for insert trigger) : ".$values."<br>";
	echo "<b>values to be updated</b>(for update trigger) : ".$upd_vals."<br>";
	
	$query = "SELECT cols.table_name, cols.column_name, cols.position, cons.status, cons.owner
		FROM all_constraints cons, all_cons_columns cols
		WHERE cols.table_name = '$table'
		AND cons.constraint_type = 'P'
		AND cons.constraint_name = cols.constraint_name
		AND cons.owner = cols.owner
		ORDER BY cols.table_name, cols.position";

	$p_key_res = oci_parse($conn, $query);
	oci_execute($p_key_res);
	$p_keys = [];
	$constr = "'";
	while($col_row = oci_fetch_array($p_key_res)) {
		$col = strtoupper($col_row['COLUMN_NAME']);
		$p_keys[] = $col;
		$constr .= " \"".$col."\"=' || :OLD.".$col." || ' and ";
		
		// echo "<br><br><br>";
	}
	$constr = substr($constr, 0, -9);
	echo "constraints: ".$constr."<br>";

	$insert_trigger = "CREATE OR REPLACE TRIGGER INSERT_ON_$table AFTER INSERT ON $schema.$table ".
	                    // "REFERENCING OLD AS OLD NEW AS NEW ".
	                    "FOR EACH ROW ".
						"DECLARE ".
							"query1 VARCHAR2(1000); ".
                            "PRAGMA AUTONOMOUS_TRANSACTION; ".
						"BEGIN ".
							"query1 := 'INSERT INTO $table@$dblink(".implode(", ", $columns).") VALUES(' || ".$values." || ')'; ".
							"dbms_output.put_line('executing query: ' || query1); ".
							"EXECUTE IMMEDIATE query1; ".
							"COMMIT; ".
						"END; ";

	$delete_trigger = "CREATE OR REPLACE TRIGGER DELETE_ON_$table AFTER DELETE ON $schema.$table ".
	                    // "REFERENCING OLD AS OLD NEW AS NEW ".
	                    "FOR EACH ROW ".
						"DECLARE ".
                            "query1 VARCHAR2(1000); ".
                            "PRAGMA AUTONOMOUS_TRANSACTION; ".
						"BEGIN ".
							"query1 := 'DELETE FROM $table@$dblink WHERE ' || ". $constr."; ".
							"dbms_output.put_line('executing query: ' || query1); ".
							"EXECUTE IMMEDIATE query1; ".
							"COMMIT; ".
						"END; ";




	$update_trigger = "CREATE OR REPLACE TRIGGER UPDATE_ON_$table AFTER update ON $schema.$table ".
	                    // "REFERENCING OLD AS OLD NEW AS NEW ".
	                    "FOR EACH ROW ".
						"DECLARE ".
                            "query1 VARCHAR2(1000); ".
                            "PRAGMA AUTONOMOUS_TRANSACTION; ".
						"BEGIN ".
							"query1 := 'UPDATE $table@$dblink SET ' || ".$upd_vals." || ' WHERE ' || ".$constr."; ".
							"dbms_output.put_line('executing query: ' || query1); ".
							"EXECUTE IMMEDIATE query1;" .
							"COMMIT; ".
						"END; ";

	echo "<br>----------------------------<br>";
	echo $insert_trigger."<br>";
	echo "<br><br>";
	// echo "<br><br><br>";
	echo $delete_trigger."<br>";
	echo "<br><br>";
	// echo "<br><br><br>";
	echo $update_trigger."<br>";


	$insert_trigger_query = oci_parse($conn, $insert_trigger);
	oci_execute($insert_trigger_query);
	echo "<br><br>............";
	$delete_trigger_query = oci_parse($conn, $delete_trigger);
	oci_execute($delete_trigger_query);
	echo "<br><br>............";
	$update_trigger_query = oci_parse($conn, $update_trigger);
	oci_execute($update_trigger_query);
	echo "<br><br>***************************************<br><br>";
}

echo "Hello World";
// Close the Oracle connection
oci_close($conn);

?>
<!-- CREATE OR REPLACE TRIGGER insert_on_users AFTER INSERT ON USERS
FOR EACH ROW
BEGIN
	dbms_output.put_line('INSERT INTO users values(' || :NEW.id || ', ''' || :NEW.mail || ''', ''' || :NEW.password || ''', ' || CASE WHEN :NEW.otp IS NULL THEN 'NULL' ELSE '' || :NEW.otp || '' END || ')');
END; -->
<!-- 
CREATE OR REPLACE PROCEDURE log_sal (
  emp_id NUMBER, old_sal NUMBER, new_sal NUMBER)
AS LANGUAGE JAVA
NAME 'DBTrigger.logSal(int, float, float)';
 -->
<!-- CREATE OR REPLACE PROCEDURE proc_users (query VARCHAR2)
AS LANGUAGE JAVA
NAME 'DBTrigger.update_mysql(java.lang.String)'; -->